﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace uRouteMeUp.Extensions
{
    public class Route
    {
        public List<Position> Positions { get; set; }
        public int Zoom { get; set; }
        public double Distance { get; set; }
        public Position WindowPosition { get; set; }
    }
}
