﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace uRouteMeUp.Extensions
{
    public class Position
    {
        public string latitude { get; set; }
        public string longtitude { get; set; }
    }
}
