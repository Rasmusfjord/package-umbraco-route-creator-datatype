﻿var cordinates = {
    "Positions": [],
    "Zoom": 9,
    "Distance": 0,
    WindowPosition : {}
};
var map;
var startPosition = [55.46017083861817, 8.701171875];
$(function () {

    BtnEvents();
    GetSavedData(); //gets the data back from umbraco
    console.log(cordinates);
    if (cordinates.Positions.length != 0) {
        startPosition = [cordinates.Positions[0].latitude, cordinates.Positions[0].longtitude];
    }

    map = $("#Map");

    map.gmap3({
        map: {
            options: {
                center: startPosition,
                scaleControl: true,
                zoom: cordinates.Zoom,
                streetViewControl: false,
                disableDefaultUI: true,
                zoomControl: true
            },
            events: {
                click: function (map, event) {
                    AddMappingCordinate(event.latLng);
                    RerenderLines();

                },
                zoom_changed: function (map, marker) {
                    SaveZoomLevel(map.getZoom());
                },
                dragend: function (map, marker) {
                    var center = map.getCenter();
                    SaveCurrentWindowPosition(center);
                }
            }
        }

    });
    if ($("#Map").closest(".tabpage").css("display") == "block") {
        setTimeout(RerenderLines, 1200);

    }

});

function RerenderLines() {

    var Points = [
    ];

    $(".dataholder").val(JSON.stringify(cordinates));

    if (cordinates.Positions.length == 0) {
        map.gmap3({
            clear: {
                name: ["polyline", "marker"]
            }
        });
    }
    else {

        for (i = 0; i < cordinates.Positions.length; i++) {
            Points.push(
                [cordinates.Positions[i].latitude, cordinates.Positions[i].longtitude]
                )
        }
        map.gmap3({
            clear: {
                name: ["polyline", "marker"]
            }
        });
        map.gmap3({
            polyline: {
                options: {
                    strokeColor: "#007cd2",
                    strokeOpacity: 0.6,
                    strokeWeight: 3,
                    geodesic: true,
                    path: Points
                }
            },
            marker: {
                values: [
                    { latLng: [cordinates.Positions[0].latitude, cordinates.Positions[0].longtitude], data: "Starting point", options: { icon: "../umbraco/Plugins/uRouteMeUp/img/marker-blue.png" } },
                    { latLng: [cordinates.Positions[cordinates.Positions.length - 1].latitude, cordinates.Positions[cordinates.Positions.length - 1].longtitude], data: "Ending point", options: { icon: "../umbraco/Plugins/uRouteMeUp/img/marker-green.png" } }
                ]
            }
        })


    }
    CalculateDistance();
}
function AddMappingCordinate(latlang) {

    cordinates.Positions.push(
          { "latitude": latlang.lat(), "longtitude": latlang.lng() }
        );

}
function SaveZoomLevel(ZoomLevel) {
    cordinates.Zoom = ZoomLevel;
    $(".dataholder").val(JSON.stringify(cordinates));

}
function SaveCurrentWindowPosition(center) {
   
    cordinates.WindowPosition =  { "latitude": center.lat(), "longtitude": center.lng() }
    $(".dataholder").val(JSON.stringify(cordinates));
}
function CalculateDistance() {
    var distance = 0;
    var jqValueHolder = $("#Route-Widget #Statistics #Distance .value");

    if (cordinates.Positions.length >= 2) {
        for (var i = 1; i < cordinates.Positions.length; i++) {
            var distanceBetweenPoints = CalculateDistanceBetweenPoints(cordinates.Positions[i - 1], cordinates.Positions[i]);
            distance = distance + distanceBetweenPoints;
        }
    }

    jqValueHolder.text("");
    jqValueHolder.text(distance.toFixed(2));

    cordinates.Distance = distance.toFixed(2);
    $(".dataholder").val(JSON.stringify(cordinates));
}
function GetSavedData() {
    var dataholder = $(".dataholder");
    if (dataholder.val() != "") {
        var distanceHolder = $("#Route-Widget #Statistics #Distance .value");

        cordinates = JSON.parse(dataholder.val());

        //adding/redrawning 
        distanceHolder.text("");
        distanceHolder.text(cordinates.Distance);

    }


}

/*BUTTONS*/
function ResetRoute() {
    $("#Route-Widget .button[data-option='reset']").on("click", function () {
        if (confirm("Are you sure that you want to remove all points?")) {
            cordinates.Positions = [];
            RerenderLines();
        }
    })

    return false;

}
function RemoveLastPoint() {
    $("#Route-Widget .button[data-option='removeLastPoint']").on("click", function () {

        cordinates.Positions.pop();
        RerenderLines();

    })

    return false;

}
function TabFix() {
    $("#body_TabView1 a").on("click", function () {

        if ($("#Map").closest(".tabpage").css("display") == "block") {
            //Map In Tab Resize fix
            var mymap = $("#Map").gmap3('get');
            google.maps.event.trigger(mymap, 'resize');
            mymap.setZoom(mymap.getZoom());
            mymap.panToBounds(mymap.getBounds());

            map.gmap3({
                map: {
                    options: {
                        center: startPosition
                    }
                }
            });

            setTimeout(RerenderLines, 1200);
        }
    })


}
function ImportKMLFile() {
    document.getElementById("KMLfile").addEventListener("change", LoadKML, false);

    $("#Route-Widget .button[data-option='importKMLfile']").on("click", function () {
        var fileInput = $('input[class="KMLfile"]');
        fileInput.trigger('click');

    })
}

//Load KML runs on file load
function LoadKML(e) {
    var files = e.target.files;
    var reader = new FileReader();

    reader.onload = function () {
        cordinates.Positions = [];
        RerenderLines();    
        var xml = new DOMParser().parseFromString(this.result, "text/xml");

        $.each(toGeoJSON.kml(xml).features[0].geometry.coordinates, function (index, value) {
          
            cordinates.Positions.push(
              { "latitude": value[1], "longtitude": value[0] }
            );

        });
   
        //Pos at center
        if (cordinates.Positions.length != 0) {
            startPosition = [cordinates.Positions[0].latitude, cordinates.Positions[0].longtitude];
        }
        map.gmap3({
            map: {
                options: {
                    center: startPosition
                }
            }
        });
     
        RerenderLines();
        CalculateDistance();
        console.log(cordinates);
    };

    reader.readAsText(files[0]);
}


function BtnEvents() {
    ResetRoute();
    RemoveLastPoint();
    TabFix();
    ImportKMLFile();
}

/*HELPERS*/
function CalculateDistanceBetweenPoints(a, b) {

    var p1 = new google.maps.LatLng(a.latitude, a.longtitude);
    var p2 = new google.maps.LatLng(b.latitude, b.longtitude);
    var val = (google.maps.geometry.spherical.computeDistanceBetween(p1, p2) / 1000);

    return val;
}


/*JSON EXAMPLE*/
/*
 * "Positions": [
            { "lat": 55.46017083861817, "long": 8.701171875 },
            { "lat": 55.46017083861817, "long": 8.701171875 }
    ],
    "Zoom": 1,
    "Distance":0
 * 
 */