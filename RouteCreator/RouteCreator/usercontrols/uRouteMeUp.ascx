﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="uRouteMeUp.ascx.cs" Inherits="uRouteMeUp.usercontrols.RoutePlanner" %>
<link rel="stylesheet" href="../umbraco/Plugins/uRouteMeUp/css/master.css">

<div id="Route-Widget">
    <div id="Map"></div>
    <div id="Options">
        <a class="button" data-option="reset">Reset</a>
        <a class="button" data-option="removeLastPoint">Remove point</a>
        <span class="seperator">&nbsp;</span>
        <a class="button" data-option="importKMLfile">Import KML</a>   
    </div>
    <div id="Statistics">
        <span id="Distance">Distance : <span class="value">-</span> km</span>
        <span id="Legend">
            <span class="icon">Start point<img src="../umbraco/Plugins/uRouteMeUp/img/marker-blue.png" /></span>
            <span class="icon">End point<img src="../umbraco/Plugins/uRouteMeUp/img/marker-green.png" /></span>
        </span>
    </div>
</div>
<input type="file" id="KMLfile" name="KMLfile" class="KMLfile" style="position: absolute;top: -1000px;"/>
<asp:TextBox TextMode="MultiLine" ID="data" CssClass="dataholder" Text="" runat="server"></asp:TextBox>
<script src="http://maps.googleapis.com/maps/api/js?sensor=false" type="text/javascript"></script>

<script src="../umbraco/Plugins/uRouteMeUp/js/plugins.js"></script>
<script src="../umbraco/Plugins/uRouteMeUp/js/main.js"></script>
