﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace uRouteMeUp.usercontrols
{
    public partial class RoutePlanner : System.Web.UI.UserControl, umbraco.editorControls.userControlGrapper.IUsercontrolDataEditor
    {
       
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Page.IsPostBack)
            {
                data.Text = value.ToString();
            }
        }

        public object value
        {
            get
            {
                return data.Text;
            }
            set
            {
                data.Text = value.ToString();
            }
        }
    }
}