var cordinates = {
    "Positions": [],
    "Zoom": 9,
    "Distance":0
};
var map;

$(function () {
    BtnEvents();
    map = $("#Map");

    map.gmap3({
        map: {
            options: {
                center: [55.46017083861817, 8.701171875],
                scaleControl: true,
                zoom: cordinates.Zoom,
                streetViewControl: false,
                disableDefaultUI: true,
                zoomControl: true
            },
            events: {
                click: function (map, event) {
                    AddMappingCordinate(event.latLng);
                    RerenderLines();


                },
                zoom_changed: function (map, marker) {
                    SaveZoomLevel(map.getZoom());
                }
            }
        }


    });

});

function RerenderLines() {

    var Points = [

    ];
    CalculateDistance();

    if (cordinates.Positions.length == 0) {
        map.gmap3({
            clear: {
                name: ["polyline","marker"]
            }
        });
    }
    else {

        for (i = 0; i < cordinates.Positions.length; i++) {
            Points.push(
                [cordinates.Positions[i].lat, cordinates.Positions[i].long]
                )
        }
        map.gmap3({
            clear: {
                name: ["polyline", "marker"]
            }
        });
        map.gmap3({
            polyline: {
                options: {
                    strokeColor: "#007cd2",
                    strokeOpacity: 0.6,
                    strokeWeight: 3,
                    geodesic: true,
                    path: Points
                }
            },
            marker: {
                values: [
                    { latLng: [cordinates.Positions[0].lat, cordinates.Positions[0].long], data: "Starting point", options: { icon: "img/marker-blue.png" } },
                    { latLng: [cordinates.Positions[cordinates.Positions.length - 1].lat, cordinates.Positions[cordinates.Positions.length - 1].long], data: "Ending point", options: { icon: "img/marker-green.png" } }
                ]
            }
        })
    }
}
function AddMappingCordinate(latlang) {

    var isStartPoint = false;
    if (cordinates.Positions.length == 0) {
        isStartPoint = true;
    }

    cordinates.Positions.push(
          { "lat": latlang.lat(), "long": latlang.lng(), "startPoint": isStartPoint }
        );

}
function SaveZoomLevel(ZoomLevel) {
    cordinates.Zoom = ZoomLevel;

}
function CalculateDistance() {
    var distance = 0.0;
    var jqValueHolder = $("#Route-Widget #Statistics #Distance .value");
    
    if (cordinates.Positions.length >= 2) {
        for (var i = 1; i < cordinates.Positions.length; i++) {
            var distanceBetweenPoints = CalculateDistanceBetweenPoints(cordinates.Positions[i - 1], cordinates.Positions[i]);
            distance = distance + distanceBetweenPoints;
        }
    }

    jqValueHolder.text("");
    jqValueHolder.text(distance.toFixed(2));

    cordinates.Distance = distance.toFixed(2);
}

/*BUTTONS*/
function ResetRoute() {
    $("#Route-Widget .button[data-option='reset']").on("click", function () {
        if (confirm("Are you sure that you want to remove all points?")) {
            cordinates.Positions = [];
            RerenderLines();
        }
    })

    return false;

}
function RemoveLastPoint() {
    $("#Route-Widget .button[data-option='removeLastPoint']").on("click", function () {

        cordinates.Positions.pop();
        RerenderLines();

    })

    return false;

}

function BtnEvents() {
    ResetRoute();
    RemoveLastPoint();
}

/*HELPERS*/
function CalculateDistanceBetweenPoints(a, b) {
    var p1 = new google.maps.LatLng(a.lat, a.long);
    var p2 = new google.maps.LatLng(b.lat, b.long);
    var val = (google.maps.geometry.spherical.computeDistanceBetween(p1, p2) / 1000);
  
    return val;
}


/*JSON EXAMPLE*/
/*
 * "Positions": [
            { "lat": 55.46017083861817, "long": 8.701171875, "startPoint": true }
    ],
    "Zoom": 1,
    "Distance":0
 * 
 */